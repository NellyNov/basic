<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\models;

use yii\db\ActiveRecord;

/**
 * LoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class RegistForm extends ActiveRecord {

    public static function tableName() {
        return 'register';
    }

    public function rules() {
        return [
            [['name', 'email', 'data'], 'required'],
            ['name', 'string', 'length' => [2, 15]],
            ['email', 'email'],
            ['data', 'trim'],
            ['fm', 'safe'],
        ];
    }

    public function getBook() {
        return $this->hasMany(Book::className(), ['id_user' => 'id']);
    }
    
    public function getBookAsString(){
        $arr=$this->getBook()->asArray()->all();    
        
        $bookNames=[];
        foreach ($arr as $element){
            $bookNames[]=$element['name_book'];
        }
                
        return [
               'label' => 'Книги:',
               'value' => implode(', ', $bookNames),
           ];               
    }

}

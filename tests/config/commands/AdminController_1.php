<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\commands;

use Yii;
use yii\console\Controller;
use app\models\User;

class AdminController extends Controller {

    public $countuser;

    public function options($actionID) {
        return ['countuser'];
    }

    public function optionAliases() {
        return ['c' => 'countuser'];
    }

    public function actionAdd() {
        $user = new User();
        $user->username = 'admin';
        $user->password = Yii::$app->getSecurity()->generatePasswordHash('admin');
        $user->auth_key = Yii::$app->security->generateRandomString();
        $user->role = '20';
        $user->save(false);
    }

    public function actionCreate() {
        for ($x = 0; $x < $this->countuser; $x++) {
            $u = new User();
            $u->username = 'user';
            $u->password = Yii::$app->getSecurity()->generatePasswordHash('user');
            $u->auth_key = Yii::$app->security->generateRandomString();
            $u->save(false);
        }
    }

}

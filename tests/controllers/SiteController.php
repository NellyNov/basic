<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\RegistForm;
use app\models\User;
use app\models\CheckinForm;

class SiteController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'users'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['users'],
                        'allow' => true,
                        'roles' => ['@'],
                    //'matchCallback' => function ($rule, $action) {
                    //    return User::isUserAdmin(Yii::$app->user->identity->username);
                    //      }
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ]
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex() {

        $regTable = RegistForm::find()->all();
        $model = new RegistForm();


// \yii\helpers\VarDumper::dump($model,10,1);// die();
// $model->save();
        if ($model->load(Yii::$app->request->post())) {

            if ($model->save(false)) {



//\yii\helpers\VarDumper::dump($model->name,10,1); die();
                Yii::$app->session->setFlash('success', 'Данные в БД записаны');
                return $this->refresh();
            } else {
// \yii\helpers\VarDumper::dump($model->getErrors());
                Yii::$app->session->setFlash('error', 'Ошибка');
            }
        }

        return $this->render('index', [
                    'regTable' => $regTable,
                    'model' => $model
                        ]
        );
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin() {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();

        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        return $this->render('login', [
                    'model' => $model
        ]);
    }

    public function actionDelete() {
        $model = Yii::$app->request->post('delete');
        if (isset($model)) {
            (RegistForm::deleteAll(['in', 'id', $model]));
            return $this->redirect(['index']);
        } else {
            echo 'Error';
        };

        $this->redirect(['index']);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout() {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact() {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
                    'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout() {

        return $this->render('about');
    }

    public function actionUsers() {
        $users = User::find('id', 'username', 'email')->all();
        //  \yii\helpers\VarDumper::dump($users);
        //  die;

        return $this->render('users', ['users' => $users,]);
    }

    public function actionCheck() {
        $model = new CheckinForm();

        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->checkin()) {
                if (Yii::$app->getUser()->login($user)) {
                    return $this->goHome();
                }
            }
        }

        return $this->render('check', [
                    'model' => $model,
        ]);
    }

}

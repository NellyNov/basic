<?php

use yii\db\Schema;
use yii\db\Migration;

/**
 * Handles the creation of table `register`.
 */
class m171116_135442_create_register_table extends Migration {

    /**
     * @inheritdoc
     */
    public function up() {
        $this->createTable('register', [
            'id' => 'pk',
            'name' =>'string NOT NULL',
            'email' => 'string NOT NULL',
            'data' => 'date NOT NULL',
            'fm' => 'boolean NOT NULL',
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down() {
        $this->dropTable('register');
    }

}

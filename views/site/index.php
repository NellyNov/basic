<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

//use phpDocumentor\Reflection\Types\Boolean;
?>

<?php
/* @var $this yii\web\View */



$this->title = 'My Yii Application';
/* @var $regTable type */
//\yii\helpers\VarDumper::dump($reg, 10, true);
?>
<script src="//ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
<script>
    $(document).on("click", "#addForm", function () {
        var form = $("form[name='form']");
        form.css("display", "block");
        $("#addForm").replaceWith(form);

    });

    $(document).on("click", "#buttonDelete", function () {
        /* @var id integer */
        var id = $(this).attr('data-del');
        $("input[name='delete']").val(id);
        $("#formDelete").submit();
    });

</script> 



<div class="jumbotron">
    <table class="table table-striped">

        <?php foreach ($regTable as $r): ?>    
            <tr> 
                <td> <?php echo $r->id; ?>  <td>  
                <td> <?php echo $r->name; ?> <td>
                <td> <?php echo $r->email; ?> <td>
                <td> <?php echo $r->data; ?></td>
                <td> <?php echo $r->fm ? "Ж" : "М"; ?></td>
                <td> <?php //echo Html::a('Удалить', Url::to(['site/delete', 'id' => $r->id]))                     ?> </td>
                <td><button data-del="<?php echo $r->id ?>" id="buttonDelete" >Удалить</button></td>
                </td> 
            </tr>
        <?php endforeach; ?>
    </table>

    <form method = 'POST'  id="formDelete" action="<?php echo Url::to(['site/delete']) ?> " >
        <input type="hidden" name="_csrf" value="<?= Yii::$app->request->getCsrfToken() ?>" />
        <input type="hidden" name="delete" value='' />

    </form>

    <br>
    <button id="addForm" onclick="return false">Показать форму</button>
    <br>

    <?php
    $form = ActiveForm::begin(
                    ['options' => ['style' => "display: none",
                            'name' => "form"
                        ]]
            )
    ?>
    <?php echo $form->field($model, 'name')->label('Имя'); ?>
    <?php echo $form->field($model, 'email')->label('Почта'); ?>
    <?php echo $form->field($model, 'data')->label('Дата'); ?>
    <?php echo $form->field($model, 'fm')->radio(['label' => 'Ж', 'value' => 1, 'format' => 'boolean', 'uncheck' => null]); ?>
    <?php echo $form->field($model, 'fm')->radio(['label' => 'М', 'value' => 0, 'format' => 'boolean', 'uncheck' => null]); ?>
    <?php echo HTML::submitButton('Отправить') ?>
    <?php ActiveForm::end(); ?>
</div>

